package com.apd.filemanagement.repository;

import com.apd.filemanagement.model.FileManagement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FileManagementRepository extends JpaRepository<FileManagement, String> {

    @Query("SELECT f, u.username " +
            "FROM FileManagement f " +
            "JOIN f.uploadByUser u")
    List<FileManagement[]> findAllFilesWithUsername();

}