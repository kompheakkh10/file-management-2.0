package com.apd.filemanagement.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "file_management")
@Data
@RequiredArgsConstructor
@AllArgsConstructor
public
class FileManagement {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @Column(name = "f_name")
    private String fileName;

    @Column(name = "f_type")
    private String fileType;

    @Lob
    private byte[] data;

    @Column(name = "DATE_TIME")
    //@Temporal(TemporalType.TIMESTAMP)
    private String date;

    @Column(name = "DOWNLOAD_URI")
    private String fileDownloadUri;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UPLOAD_BY")
    private User uploadByUser;

    public FileManagement(String id, String fileName, String fileType, String fileDownloadUri, byte[] data, String date) {
        this.id = id;
        this.fileName = fileName;
        this.fileType = fileType;
        this.data = data;
        this.date = date;
        this.fileDownloadUri = fileDownloadUri;
    }

    public FileManagement(String fileName, String fileType, String fileDownloadUri, byte[] data, String date) {
        this.fileName = fileName;
        this.fileType = fileType;
        this.data = data;
        this.date = date;
        this.fileDownloadUri = fileDownloadUri;
    }
}
