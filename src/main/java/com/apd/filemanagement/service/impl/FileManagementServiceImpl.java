package com.apd.filemanagement.service.impl;

import com.apd.filemanagement.model.FileManagement;
import com.apd.filemanagement.exception.FileStorageException;
import com.apd.filemanagement.repository.FileManagementRepository;
import com.apd.filemanagement.service.FileManagementService;
import com.apd.filemanagement.utils.DateTimeUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class FileManagementServiceImpl implements FileManagementService {
    private FileManagementRepository fileManagementRepository;

    @Override
    public List<FileManagement[]> findAllFilesWithUsername() {
        return fileManagementRepository.findAllFilesWithUsername();
    }

    @Override
    public FileManagement postFile(MultipartFile file) {
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));

        try {
            if (fileName.contains("..")) {
                throw new FileStorageException("File name contain invalid path sequence! " + fileName);
            }

            FileManagement fileManagement = new FileManagement();
            fileManagement.setDate(DateTimeUtils.convertDateToYYYYMMDD(new Date()));

            //need to maintenance this code
            String fileDownloadUri = ServletUriComponentsBuilder
                    .fromCurrentContextPath()
                    .path("/download/")
                    .path(fileName)
                    .toUriString();

            fileManagement = new FileManagement(fileName, file.getContentType(), fileDownloadUri, file.getBytes(), fileManagement.getDate());
            return fileManagementRepository.save(fileManagement);

        } catch (IOException exception) {
            throw new FileStorageException("Could not store file: " + fileName);
        }

    }

    @Override
    public List<FileManagement> getAllFile() {

        return fileManagementRepository.findAll();
    }

    @Override
    public FileManagement getFile(String fileId) {
        return fileManagementRepository.findById(fileId).get();
    }

    @Override
    public void deleteById(String fileId) {

        FileManagement file = fileManagementRepository.findById(fileId).get();

        fileManagementRepository.delete(file);
    }

    @Override
    public List<FileManagement> postMultipleFile(MultipartFile[] files) {

        return Arrays.asList(files)
                .stream()
                .map(file -> postFile(file))
                .collect(Collectors.toList());
    }

    @Override
    public void updateFile(FileManagement file) {
        fileManagementRepository.save(file);
    }
}