package com.apd.filemanagement.service.impl;

import com.apd.filemanagement.model.Roles;
import com.apd.filemanagement.model.User;
import com.apd.filemanagement.payload.UserDto;
import com.apd.filemanagement.repository.RoleRepository;
import com.apd.filemanagement.repository.UserRepository;
import com.apd.filemanagement.service.UserService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    private RoleRepository roleRepository;

    private PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepository userRepository,
                           PasswordEncoder passwordEncoder,
                           RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
    }

    @Override
    public void saveUser(UserDto userDto) {
        User user = new User();
        user.setUsername(userDto.getFirstName() + " " + userDto.getLastName());
        user.setEmail(userDto.getEmail());

        //encrypt the password using spring security
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));

        Roles roles = roleRepository.findByName("ROLE_ADMIN");
        if (roles == null) {
            roles = checkExistRole();
        }
        user.setRoles(Arrays.asList(roles));
        userRepository.save(user);
    }

    private UserDto mapToUserDto(User users) {
        UserDto userDto = new UserDto();
        String[] str = users.getUsername().split(" ");
        userDto.setFirstName(str[0]);
        userDto.setLastName(str[1]);
        userDto.setEmail(users.getEmail());

        return userDto;
    }

    private Roles checkExistRole() {
        Roles roles = new Roles();
        roles.setName("ROLE_ADMIN");

        return roleRepository.save(roles);
    }

    @Override
    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public List<UserDto> findAllUser() {
        return null;
    }
}