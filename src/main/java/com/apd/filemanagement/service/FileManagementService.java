package com.apd.filemanagement.service;

import com.apd.filemanagement.model.FileManagement;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface FileManagementService {

    List<FileManagement[]> findAllFilesWithUsername();

    FileManagement postFile(MultipartFile file);

    List<FileManagement> getAllFile();

    FileManagement getFile(String fileId);

    void deleteById(String id);

    List<FileManagement> postMultipleFile(MultipartFile[] files);

    void updateFile(FileManagement file);
}