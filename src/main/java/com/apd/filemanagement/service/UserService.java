package com.apd.filemanagement.service;

import com.apd.filemanagement.model.User;
import com.apd.filemanagement.payload.UserDto;

import java.util.List;

public interface UserService {

    void saveUser(UserDto userDto);

    User findUserByEmail(String email);

    List<UserDto> findAllUser();

}