package com.apd.filemanagement.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;


@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class FileManagementResponse {
    private String fileDownloadUri;
    private String fileName;
    private String fileType;
    private long size;
}
