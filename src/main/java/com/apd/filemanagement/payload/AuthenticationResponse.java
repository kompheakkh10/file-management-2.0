package com.apd.filemanagement.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import java.io.Serializable;

@AllArgsConstructor
@Data
public class AuthenticationResponse implements Serializable {

    private final String jwt;

}
