package com.apd.filemanagement.controller;

import com.apd.filemanagement.model.FileManagement;
import com.apd.filemanagement.payload.FileManagementResponse;
import com.apd.filemanagement.service.FileManagementService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api")
@AllArgsConstructor
public class FileManagementController {

    private FileManagementService fileManagementService;

    //handler for return upload-file template
    @GetMapping("uploadFile")
    public String postFile(Model model) {
        FileManagementResponse fileManagementResponse = new FileManagementResponse();

        model.addAttribute("file", fileManagementResponse);

        return "upload-file";
    }

    //handler method to save file to the database
    @PostMapping("/singleFile")
    public String uploadFile(@ModelAttribute("file") @RequestParam("file") MultipartFile file) {

        fileManagementService.postFile(file);

        return "redirect:/api";
    }

    @GetMapping()
    public String getAllFile(Model model) {

        List<FileManagement> files = fileManagementService.getAllFile().stream().map(dbFile -> {

/*            String fileDownloadUri = ServletUriComponentsBuilder
                    .fromCurrentContextPath()
                    .path("/download/".path(dbFile.getId()
                            .toUriString();*/

            return new FileManagement(
                    dbFile.getId(),
                    dbFile.getFileName(),
                    dbFile.getFileType(),
                    dbFile.getFileDownloadUri(),
                    dbFile.getData(),
                    dbFile.getDate());

        }).collect(Collectors.toList());

        List<FileManagement[]> allFiles = fileManagementService.findAllFilesWithUsername();

        model.addAttribute("files", files);

        return "file-dashboard";
    }

    @GetMapping("/{fileId}/delete")
    public String deleteFile(@PathVariable("fileId") String fileId) {
        fileManagementService.deleteById(fileId);

        return "redirect:/api";
    }

    //handler method to response upload-multiple-file to client
    @GetMapping("/uploadMultipleFile")
    public String uploadMultipleFile(Model model) {
        return "upload-multiple-file";
    }

    //handler method for handle save multiple file to database
    @PostMapping("")
    public String saveUploadMultipleFile(@ModelAttribute("files") @RequestParam("files") MultipartFile[] files) {
        fileManagementService.postMultipleFile(files);

        return "redirect:/api";
    }

    //this block of code need to maintenance
    @GetMapping("/{fileName:.+}")
    public String downloadFile(@PathVariable("fileName") String fileName, Model model) {
        FileManagement fileManagement = fileManagementService.getFile(fileName);
        ResponseEntity<byte[]> responseEntity = ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(fileManagement.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileManagement.getFileName() + "\"")
                .body(fileManagement.getData());

        model.addAttribute("downloadLink", responseEntity);

        return "redirect:/api";
    }

    @GetMapping("/{id}/view")
    public String viewFile(Model model,
                           @PathVariable("id") String fileId) {
        FileManagement file = fileManagementService.getFile(fileId);
        model.addAttribute("file", file);

        return "view-file";
    }

    //handler for edit request and return template form for update the file
    @GetMapping("/{id}/edit")
    public String editFile(@PathVariable("id") String fileId,
                           Model model) {
        FileManagement file = fileManagementService.getFile(fileId);
        model.addAttribute("file", file);

        return "edit-file";
    }

    @PostMapping("/{fileId}")
    public String updateStudent(Model model,
                                @PathVariable("fileId") String fileId,
                                @ModelAttribute("file") FileManagement file,
                                BindingResult result) {

        if (result.hasErrors()) {
            model.addAttribute("file", file);
            return "edit-file";
        }

        //create this reference for get the data field and update to the database
        FileManagement fileManagement = fileManagementService.getFile(fileId);

        file.setId(fileId);
        file.setData(fileManagement.getData());
        fileManagementService.updateFile(file);

        return "redirect:/api";
    }
}